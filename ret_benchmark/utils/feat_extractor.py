# Copyright (c) Malong Technologies Co., Ltd.
# All rights reserved.
#
# Contact: github@malong.com
#
# This source code is licensed under the LICENSE file in the root directory of this source tree.

import torch
import tensorflow as tf
import numpy as np
from tqdm import tqdm


def feat_extractor(model, data_loader, logger=None):
    model.eval()
    feats = list()

    for i, (images, targets) in enumerate(tqdm(data_loader)):

        with torch.no_grad():
            out = model(images.cuda()).data.cpu().numpy()
            feats.append(out)

        if logger is not None and (i + 1) % 100 == 0:
            logger.debug(f'Extract Features: [{i + 1}/{len(data_loader)}]')
        del out
    feats = np.vstack(feats)
    return feats

def feat_extractor_tf(model, data_loader, logger=None):
    feats = list()

    @tf.function
    def test_step(inputs):
        pred = model(inputs)

        return pred

    print("Start extract features")
    for i, (images, targets) in enumerate(tqdm(data_loader)):
        # out = model(images.numpy()).numpy()
        out = test_step(images.numpy())
        feats.append(out)

        if logger is not None and (i + 1) % 100 == 0:
            logger.debug(f'Extract Features: [{i + 1}/{len(data_loader)}]')
        del out
    print("End extract features")
    feats = np.vstack(feats)
    return feats
