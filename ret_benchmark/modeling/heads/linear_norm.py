# Copyright (c) Malong Technologies Co., Ltd.
# All rights reserved.
#
# Contact: github@malong.com
#
# This source code is licensed under the LICENSE file in the root directory of this source tree.

import torch
from torch import nn

from ret_benchmark.modeling.registry import HEADS
from ret_benchmark.utils.init_methods import weights_init_kaiming

import tensorflow as tf


@HEADS.register("linear_norm")
class LinearNorm(nn.Module):
    def __init__(self, cfg, in_channels):
        super(LinearNorm, self).__init__()
        self.fc = nn.Linear(in_channels, cfg.MODEL.HEAD.DIM)
        self.fc.apply(weights_init_kaiming)

    def forward(self, x):
        x = self.fc(x)
        x = nn.functional.normalize(x, p=2, dim=1)
        return x


@HEADS.register("linear_norm_tf")
class LinearNorm_tf(tf.keras.layers.Layer):
    def __init__(self, cfg, in_channels):
        super(LinearNorm_tf, self).__init__()
        self.fc = tf.keras.layers.Dense(
            cfg.MODEL.HEAD.DIM, use_bias=True, kernel_initializer=tf.keras.initializers.he_normal()
        )

    def call(self, x):
        x = self.fc(x)
        x = tf.nn.l2_normalize(x, axis=-1)
        return x
