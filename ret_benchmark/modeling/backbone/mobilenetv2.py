from __future__ import absolute_import, division, print_function

import torch
import torch.nn as nn
import torchvision.models as models
from torchsummary import summary

from ret_benchmark.modeling import registry

import tensorflow as tf


@registry.BACKBONES.register("mobilenetv2")
class MobileNetV2(nn.Module):
    def __init__(self):
        super(MobileNetV2, self).__init__()
        self.model = models.mobilenet_v2(pretrained=True)
        self.avg_pool = torch.nn.AdaptiveAvgPool2d(output_size=(1, 1))
        # summary(self.model.cuda(), (3, 224, 224))

        for module in filter(lambda m: type(m) == nn.BatchNorm2d, self.model.modules()):
            module.eval()
            module.train = lambda _: None

    def forward(self, x):
        x = self.model.features(x)
        x = self.avg_pool(x)

        x = x.view(x.size(0), -1)
        return x

    def load_param(self, model_path):
        param_dict = torch.load(model_path)
        for i in param_dict:
            if "last_linear" in i:
                continue
            self.model.state_dict()[i].copy_(param_dict[i])


@registry.BACKBONES.register("mobilenetv2_tf")
class MobileNetV2_tf(tf.keras.layers.Layer):
    def __init__(self):
        super(MobileNetV2_tf, self).__init__()
        self.model = tf.keras.applications.MobileNetV2(include_top=False)
        self.avgpool = tf.keras.layers.GlobalAveragePooling2D()

        # self.model.summary()

    def call(self, x):
        x = self.model(x)
        x = self.avgpool(x)

        return x

    def load_param(self, model_path):
        pass

