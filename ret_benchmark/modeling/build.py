# Copyright (c) Malong Technologies Co., Ltd.
# All rights reserved.
#
# Contact: github@malong.com
#
# This source code is licensed under the LICENSE file in the root directory of this source tree.


import os
from collections import OrderedDict

import torch
from torch.nn.modules import Sequential

from .backbone import build_backbone
from .heads import build_head

import torchvision.models as models
from torchsummary import summary

import tensorflow as tf


def build_model(cfg):
    backbone = build_backbone(cfg)
    head = build_head(cfg)

    model = Sequential(OrderedDict([("backbone", backbone), ("head", head)]))

    summary(model.cuda(), (3, cfg["INPUT"]["CROP_SIZE"], cfg["INPUT"]["CROP_SIZE"]))
    return model


def build_model_tf(cfg):
    x = tf.keras.Input(
        shape=[cfg["INPUT"]["CROP_SIZE"], cfg["INPUT"]["CROP_SIZE"], 3], name="input_image"
    )
    backbone = build_backbone(cfg)(x)
    head = build_head(cfg)(backbone)

    model = tf.keras.Model(inputs = x, outputs = head)
    model.summary()

    return model