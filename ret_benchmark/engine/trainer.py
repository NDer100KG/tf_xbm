# Copyright (c) Malong Technologies Co., Ltd.
# All rights reserved.
#
# Contact: github@malong.com
#
# This source code is licensed under the LICENSE file in the root directory of this source tree.

import datetime
import time

import numpy as np
import torch
import tensorflow as tf

from ret_benchmark.data.evaluations import RetMetric
from ret_benchmark.utils.feat_extractor import feat_extractor, feat_extractor_tf
from ret_benchmark.utils.freeze_bn import set_bn_eval
from ret_benchmark.utils.metric_logger import MetricLogger

from tqdm import tqdm


def do_train(
    cfg,
    model,
    train_loader,
    val_loader,
    optimizer,
    scheduler,
    criterion,
    checkpointer,
    writer,
    accuracy_calculator,
    device,
    checkpoint_period,
    arguments,
    logger,
):
    logger.info("Start training")
    meters = MetricLogger(delimiter="  ")
    max_iter = len(train_loader)

    start_iter = arguments["iteration"]
    best_iteration = -1
    best_mapr = 0

    start_training_time = time.time()
    end = time.time()
    for iteration, (images, targets) in enumerate(train_loader, start_iter):

        if iteration % cfg.VALIDATION.VERBOSE == 0 or iteration == max_iter:
            model.eval()
            logger.info("Validation")
            labels = val_loader.dataset.label_list
            labels = np.array([int(k) for k in labels])
            feats = feat_extractor(model, val_loader, logger=logger)

            metrics = accuracy_calculator.get_accuracy(feats, feats, labels, labels, True)
            mapr_curr = metrics["mean_average_precision_at_r"]
            metrics["R_1"] = RetMetric(feats=feats, labels=labels).recall_k(1)

            for k, v in metrics.items():
                writer.add_scalar("eval/" + k, v, iteration)

            if mapr_curr > best_mapr:
                best_mapr = mapr_curr
                best_iteration = iteration
                logger.info(f"Best iteration {iteration}: {metrics}")
                checkpointer.save(f"best_model")
            else:
                logger.info(f"Performance at iteration {iteration:06d}: {metrics}")

        model.train()
        model.apply(set_bn_eval)

        data_time = time.time() - end
        iteration = iteration + 1
        arguments["iteration"] = iteration

        scheduler.step()

        images = images.to(device)
        targets = torch.stack([target.to(device) for target in targets])

        feats = model(images)
        loss = criterion(feats, targets)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        batch_time = time.time() - end
        end = time.time()
        meters.update(time=batch_time, data=data_time, loss=loss.item())

        eta_seconds = meters.time.global_avg * (max_iter - iteration)
        eta_string = str(datetime.timedelta(seconds=int(eta_seconds)))

        if iteration % 20 == 0 or iteration == max_iter:
            logger.info(
                meters.delimiter.join(
                    [
                        "eta: {eta}",
                        "iter: {iter}",
                        "{meters}",
                        "lr: {lr:.6f}",
                        "max mem: {memory:.1f} GB",
                    ]
                ).format(
                    eta=eta_string,
                    iter=iteration,
                    meters=str(meters),
                    lr=optimizer.param_groups[0]["lr"],
                    memory=torch.cuda.max_memory_allocated() / 1024.0 / 1024.0 / 1024.0,
                )
            )
            writer.add_scalar("loss/loss", loss.item(), iteration)

        if iteration % checkpoint_period == 0:
            checkpointer.save("model_{:06d}".format(iteration))

    total_training_time = time.time() - start_training_time
    total_time_str = str(datetime.timedelta(seconds=total_training_time))
    logger.info(
        "Total training time: {} ({:.4f} s / it)".format(
            total_time_str, total_training_time / (max_iter)
        )
    )

    logger.info(f"Best iteration: {best_iteration :06d} | best mapr {best_mapr} ")


def do_train_tf(
    cfg,
    model,
    train_loader,
    val_loader,
    optimizer,
    scheduler,
    criterion,
    ckpt_manager,
    summary_writer,
    tb_save_path,
    accuracy_calculator,
    arguments,
    logger,
):
    logger.info("Start training")
    max_iter = len(train_loader)

    best_iteration = -1
    best_mapr = 0

    @tf.function
    def train_step(images, targets):
        with tf.GradientTape() as tape:
            pred = model(images)
            loss = criterion(pred, targets)

        grads = tape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(grads, model.trainable_variables))

        return loss

    pbar = tqdm(train_loader)
    for iteration, (images, targets) in enumerate(pbar):

        if iteration % cfg.VALIDATION.VERBOSE == 0 or iteration == max_iter:
            logger.info("Validation")
            labels = val_loader.dataset.label_list
            labels = np.array([int(k) for k in labels])
            feats = feat_extractor_tf(model, val_loader, logger=logger)

            metrics = accuracy_calculator.get_accuracy(feats, feats, labels, labels, True)
            mapr_curr = metrics["mean_average_precision_at_r"]
            metrics["R_1"] = RetMetric(feats=feats, labels=labels).recall_k(1)

            with summary_writer.as_default():
                for k, v in metrics.items():
                    tf.summary.scalar("eval/" + k, v, step=iteration)

            if mapr_curr > best_mapr:
                best_mapr = mapr_curr
                best_iteration = iteration
                logger.info(f"Best iteration {iteration}: {metrics}")
                model.save_weights(tb_save_path + "/best_model")
            else:
                logger.info(f"Performance at iteration {iteration:06d}: {metrics}")

        loss = train_step(images.numpy(), targets.numpy())

        if iteration % 20 == 0 or iteration == max_iter:
            # print("Iter {}/{}, loss: {:.2f}".format(iteration, max_iter, loss.numpy()))
            pbar.set_description("loss: {:.2f}".format(loss.numpy()))

            with summary_writer.as_default():
                tf.summary.scalar("loss/loss", loss.numpy(), step=iteration)

        if iteration % cfg.SOLVER.CHECKPOINT_PERIOD == 0:
            ckpt_manager.save()