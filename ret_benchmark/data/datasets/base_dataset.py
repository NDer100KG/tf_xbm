# Copyright (c) Malong Technologies Co., Ltd.
# All rights reserved.
#
# Contact: github@malong.com
#
# This source code is licensed under the LICENSE file in the root directory of this source tree.

import os, cv2
import re
from collections import defaultdict

from torch.utils.data import Dataset
from ret_benchmark.utils.img_reader import read_image

import numpy as np

class BaseDataSet(Dataset):
    """
    Basic Dataset read image path from img_source
    img_source: list of img_path and label
    """

    def __init__(self, img_source, transforms=None, mode="RGB", data_format="NCHW", framework = "pytorch"):
        self.mode = mode
        self.transforms = transforms
        self.root = os.path.dirname(img_source)
        assert os.path.exists(img_source), f"{img_source} NOT found."
        self.img_source = img_source

        self.label_list = list()
        self.path_list = list()
        self._load_data()
        self.label_index_dict = self._build_label_index_dict()

        self.framework = framework
        if self.framework == "pytorch":
            self.mean = [0.485, 0.456, 0.406]
            self.std = [0.229, 0.224, 0.225]

    def __len__(self):
        return len(self.label_list)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return (
            f"| Dataset Info |datasize: {self.__len__()}|num_labels: {len(set(self.label_list))}|"
        )

    def _load_data(self):
        with open(self.img_source, "r") as f:
            for line in f:
                _path, _label = re.split(r",| ", line.strip())
                self.path_list.append(_path)
                self.label_list.append(_label)

    def _build_label_index_dict(self):
        index_dict = defaultdict(list)
        for i, label in enumerate(self.label_list):
            index_dict[label].append(i)
        return index_dict

    # def read_image(self, image_path):
    #     image = cv2.imread(image_path)

    #     if self.mode == "RGB":
    #         image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    #     return image

    # def normalize(self, image):
    #     # if self.mode == "RGB":
    #     #     image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    #     if self.framework == "pytorch":
    #         image = image / 255.
    #         image = (image - self.mean) / self.std
    #     elif self.framework == "tensorflow":
    #         image = image / 127.5 - 1

    #     return image

    def __getitem__(self, index):
        path = self.path_list[index]
        img_path = os.path.join(self.root, path)
        label = self.label_list[index]

        img = read_image(img_path, self.mode)
        if self.transforms is not None:
            img = self.transforms(img)

        if self.framework == "tensorflow":
            img = np.transpose(img, [1, 2, 0])

        return img, label
