# Copyright (c) Malong Technologies Co., Ltd.
# All rights reserved.
#
# Contact: github@malong.com
#
# This source code is licensed under the LICENSE file in the root directory of this source tree.

import torch
from torch import nn

from ret_benchmark.losses.registry import LOSS

import tensorflow as tf


@LOSS.register("ms_loss")
class MultiSimilarityLoss(nn.Module):
    def __init__(self, cfg):
        super(MultiSimilarityLoss, self).__init__()
        self.thresh = 0.5
        self.margin = 0.1

        self.scale_pos = cfg.LOSSES.MULTI_SIMILARITY_LOSS.SCALE_POS
        self.scale_neg = cfg.LOSSES.MULTI_SIMILARITY_LOSS.SCALE_NEG

    def forward(self, feats, labels):
        assert feats.size(0) == labels.size(
            0
        ), f"feats.size(0): {feats.size(0)} is not equal to labels.size(0): {labels.size(0)}"
        batch_size = feats.size(0)
        sim_mat = torch.matmul(feats, torch.t(feats))

        epsilon = 1e-5
        loss = list()

        for i in range(batch_size):
            pos_pair_ = sim_mat[i][labels == labels[i]]
            pos_pair_ = pos_pair_[pos_pair_ < 1 - epsilon]
            neg_pair_ = sim_mat[i][labels != labels[i]]

            neg_pair = neg_pair_[neg_pair_ + self.margin > min(pos_pair_)]
            pos_pair = pos_pair_[pos_pair_ - self.margin < max(neg_pair_)]

            if len(neg_pair) < 1 or len(pos_pair) < 1:
                continue

            # weighting step
            pos_loss = (
                1.0
                / self.scale_pos
                * torch.log(1 + torch.sum(torch.exp(-self.scale_pos * (pos_pair - self.thresh))))
            )
            neg_loss = (
                1.0
                / self.scale_neg
                * torch.log(1 + torch.sum(torch.exp(self.scale_neg * (neg_pair - self.thresh))))
            )
            loss.append(pos_loss + neg_loss)

        if len(loss) == 0:
            return torch.zeros([], requires_grad=True)

        loss = sum(loss) / batch_size
        return loss


@LOSS.register("ms_loss_tf")
class MultiSimilarityLoss_tf(tf.keras.layers.Layer):
    def __init__(self, cfg):
        super(MultiSimilarityLoss_tf, self).__init__()
        self.thresh = 0.5
        self.margin = 0.1
        self.epsilon = 1e-5

        self.scale_pos = cfg.LOSSES.MULTI_SIMILARITY_LOSS.SCALE_POS
        self.scale_neg = cfg.LOSSES.MULTI_SIMILARITY_LOSS.SCALE_NEG
        self.hard_mining = True

    def call(self, feats, labels):
        N = tf.unstack(tf.shape(labels))[0]

        labels = tf.reshape(labels, [-1, 1])
        adjacency = tf.equal(labels, tf.transpose(labels))
        adjacency_not = tf.logical_not(adjacency)

        mask_pos = tf.cast(adjacency, dtype=tf.float32)
        mask_neg = tf.cast(adjacency_not, dtype=tf.float32)

        sim_mat = tf.matmul(feats, feats, transpose_a=False, transpose_b=True)

        pos_mat = tf.multiply(sim_mat, mask_pos)
        mask_pos = mask_pos * tf.cast(tf.less(pos_mat, 1 - self.epsilon), tf.float32)

        neg_mat = tf.multiply(sim_mat, mask_neg)
        if self.hard_mining:
            pos_mat = tf.multiply(pos_mat, mask_pos)

            pos_max = tf.reduce_max(pos_mat, axis=1, keepdims=True)
            pos_min = tf.reduce_min((sim_mat - pos_max) * mask_pos, axis=1, keepdims=True) + pos_max
            pos_min = tf.tile(pos_min, [1, N])
            neg_max = tf.tile(tf.reduce_max(neg_mat, axis=1, keepdims=True), [1, N])

            neg_mat = neg_mat * tf.cast(tf.greater(neg_mat + self.margin, pos_min), tf.float32)
            pos_mat = pos_mat * tf.cast(tf.less(pos_mat - self.margin, neg_max), tf.float32)

        pos_loss = (
            1.0
            / self.scale_pos
            * tf.math.log(
                1
                + tf.reduce_sum(
                    tf.multiply(tf.math.exp(-self.scale_pos * (pos_mat - self.thresh)), mask_pos),
                    axis=1,
                )
            )
        )

        neg_loss = (
            1.0
            / self.scale_neg
            * tf.math.log(
                1
                + tf.reduce_sum(
                    tf.multiply(tf.math.exp(self.scale_neg * (neg_mat - self.thresh)), mask_neg),
                    axis=1,
                )
            )
        )

        ## filter loss with 0
        valid_mask = tf.cast(tf.greater(neg_loss * pos_loss, 0), tf.float32)
        # valid_num = tf.reduce_sum(valid_mask)

        loss = tf.reduce_mean(valid_mask * (pos_loss + neg_loss))
        return loss
