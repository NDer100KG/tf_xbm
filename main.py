# Copyright (c) Malong Technologies Co., Ltd.
# All rights reserved.
#
# Contact: github@malong.com
#
# This source code is licensed under the LICENSE file in the root directory of this source tree.
import os, shutil, yaml

import argparse
import torch
import tensorflow as tf

from ret_benchmark.config import cfg
from ret_benchmark.data import build_data
from ret_benchmark.engine.trainer import do_train, do_train_tf
from ret_benchmark.losses import build_loss
from ret_benchmark.modeling import build_model, build_model_tf
from ret_benchmark.solver import (
    build_lr_scheduler,
    build_optimizer,
    build_optimizer_tf,
    build_lr_scheduler_tf,
)
from ret_benchmark.utils.logger import setup_logger
from ret_benchmark.utils.checkpoint import Checkpointer
from ret_benchmark.utils.gpu_config import set_memory_growth

from tensorboardX import SummaryWriter
from pytorch_metric_learning.utils.accuracy_calculator import AccuracyCalculator


def train(cfg):
    logger = setup_logger(name="Train", level=cfg.LOGGER.LEVEL)
    logger.info(cfg)
    model = build_model(cfg)
    device = torch.device(cfg.MODEL.DEVICE)
    model.to(device)

    criterion = build_loss(cfg)

    optimizer = build_optimizer(cfg, model)
    scheduler = build_lr_scheduler(cfg, optimizer)

    train_loader = build_data(cfg, is_train=True)
    val_loader = build_data(cfg, is_train=False)

    logger.info(train_loader.dataset)
    logger.info(val_loader.dataset)

    arguments = dict()
    arguments["iteration"] = 0

    checkpoint_period = cfg.SOLVER.CHECKPOINT_PERIOD
    tb_save_path = os.path.join(cfg.SAVE_DIR, cfg.NAME)
    checkpointer = Checkpointer(model, optimizer, scheduler, tb_save_path)
    writer = SummaryWriter(tb_save_path)

    accuracy_calculator = AccuracyCalculator(
        include=("precision_at_1", "mean_average_precision_at_r", "r_precision"), exclude=()
    )

    do_train(
        cfg,
        model,
        train_loader,
        val_loader,
        optimizer,
        scheduler,
        criterion,
        checkpointer,
        writer,
        accuracy_calculator,
        device,
        checkpoint_period,
        arguments,
        logger,
    )


def train_tf(cfg):
    logger = setup_logger(name="Train", level=cfg.LOGGER.LEVEL)
    logger.info(cfg)

    set_memory_growth()  ## Important step

    model = build_model_tf(cfg)

    criterion = build_loss(cfg)

    optimizer = build_optimizer_tf(cfg, model)
    scheduler = build_lr_scheduler_tf(cfg, optimizer)

    train_loader = build_data(cfg, is_train=True)
    val_loader = build_data(cfg, is_train=False)

    logger.info(train_loader.dataset)
    logger.info(val_loader.dataset)

    arguments = dict()
    arguments["iteration"] = 0

    tb_save_path = os.path.join(cfg.SAVE_DIR, cfg.NAME)
    summary_writer = tf.summary.create_file_writer(tb_save_path)

    global_step = tf.Variable(1)

    checkpoint = tf.train.Checkpoint(
        optimizer=optimizer,
        model=model,
        step=global_step,
    )
    ckpt_manager = tf.train.CheckpointManager(checkpoint, tb_save_path, max_to_keep=10)

    accuracy_calculator = AccuracyCalculator(
        include=("precision_at_1", "mean_average_precision_at_r", "r_precision"), exclude=()
    )

    do_train_tf(
        cfg,
        model,
        train_loader,
        val_loader,
        optimizer,
        scheduler,
        criterion,
        ckpt_manager,
        summary_writer,
        tb_save_path,
        accuracy_calculator,
        arguments,
        logger,
    )


def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(description="Train a retrieval network")
    parser.add_argument("--cfg", dest="cfg_file", help="config file", default=None, type=str)
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    cfg.merge_from_file(args.cfg_file)

    devices = ""
    for d in cfg["CUDA_VISIBLE_DEVICES"]:
        devices = devices + str(d) + ','
    os.environ["CUDA_VISIBLE_DEVICES"] = devices

    tb_save_path = os.path.join(cfg.SAVE_DIR, cfg.NAME)
    os.makedirs(tb_save_path, exist_ok=True)

    with open(os.path.join(tb_save_path, "config.yml"), "w") as outfile:
        yaml.dump(cfg, outfile)

    if cfg.FRAMEWORK == "pytorch":
        train(cfg)
    else:
        train_tf(cfg)
