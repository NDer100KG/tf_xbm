#!/bin/bash

LOG_DIR="logs"
if [[ ! -d "${LOG_DIR}" ]]; then
    echo "Creating log dir for training : ${LOG_DIR}"
    mkdir ${LOG_DIR}
fi

python main.py --cfg configs/train_tensorflow_mbv2.yaml
