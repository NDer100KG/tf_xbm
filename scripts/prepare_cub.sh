#!/bin/bash
set -e

CUB_ROOT='resource/datasets/CUB_200_2011/'
CUB_DATA='https://drive.google.com/u/0/uc?id=1hbzc_P1FuxMkcabkgn9ZKinBwW683j45&export=download'


if [[ ! -d "${CUB_ROOT}" ]]; then
    mkdir -p resource/datasets
    pushd resource/datasets
    echo "Downloading CUB_200_2011 data-set..."
    gdown ${CUB_DATA}
    tar -zxf CUB_200_2011.tgz
    rm -rf CUB_200_2011.tgz
    popd
fi
# Generate train.txt and test.txt splits
echo "Generating the train.txt/test.txt split files"
python scripts/split_cub_for_ms_loss.py


