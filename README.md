# Tensorflow and Pytorh codes for MS-Loss and XBM
 

Tensorflow and Pytorh codes for Multi-Similarity Loss for Deep Metric Learning and Cross-Batch Memory for Embedding Learning

:rage3: Important:  XBM is ongoing, not done yet

Papers: [MSLoss](http://openaccess.thecvf.com/content_CVPR_2019/papers/Wang_Multi-Similarity_Loss_With_General_Pair_Weighting_for_Deep_Metric_Learning_CVPR_2019_paper.pdf) / [XBM](https://arxiv.org/pdf/1912.06798.pdf)

Official codes (pytorch): [MalongTech/research-ms-loss](https://github.com/MalongTech/research-ms-loss) / [MalongTech/research-xbm](https://github.com/MalongTech/research-xbm)

***
- [Tensorflow and Pytorh codes for MS-Loss and XBM](#tensorflow-and-pytorh-codes-for-ms-loss-and-xbm)
  - [Installation](#installation)
  - [Data Preparation](#data-preparation)
  - [Config settings](#config-settings)
  - [Training and Testing](#training-and-testing)
  - [Benchmarks](#benchmarks)
  - [References](#references)
  - [Licenses](#licenses)

****

## Installation

### Conda
```
conda env create -f environment.yml
conda activate tf_XBM
```

### pip
```
pip install -r requirements.txt
```

***

##  Data Preparation

``` bash
./scripts/prepare_cub.sh
```

***

## Config settings

* mean and std make much influence on accuracy if pretrained model is used, the preprocessing formula is like `((x/255.) - mean) / std`.

* Table below shows default settings for differnet models (corresponding to how they were trained)

    | model       | framework  | format | mean                     | std                                     |
    | ----------- | ---------- | ------ | ------------------------ | --------------------------------------- |
    | resnet50    | pytorch    | RGB    | [0.485, 0.456, 0.406]    | [0.229, 0.224, 0.225]                   |
    | mobilenetV2 | pytorch    | RGB    | [0.485, 0.456, 0.406]    | [0.229, 0.224, 0.225]                   |
    | resnet50    | tensorflow | BGR    | [0.4076, 0.4579, 0.4850] | [0.003921569, 0.003921569, 0.003921569] |
    | mobilenetV2 | tensorflow | RGB    | [0.5, 0.5, 0.5]          | [0.5, 0.5, 0.5]                         |

* training batch size: The bigger the better!

***

## Training and Testing
    

:point_right: Pytorch
 
```
./scripts/run_cub_pytorch.sh
```

:point_right: Tensorflow

```
./scripts/run_cub_tensorflow.sh
```

* Note: since I only own a GTX1060 6GB GPU, I set a small batch size in config files, you can make it bigger if more GPU mem is available
* Note: tensorboard logs will be saved in `logs/`

*** 


## Benchmarks
Note: all models are trained under resolution of 160

|                | batch_size | MAPR   | Precision@1 | r_precision | Recall@1 |
| -------------- | ---------- | ------ | ----------- | ----------- | -------- |
| torch_resnet50 | pretrained | 0.1089 | 0.4443      | 0.2069      | 0.4443   |
| TF_resnet50    | pretrained | 0.1010 | 0.4343      | 0.1954      | 0.4343   |
| torch_MBv2     | pretrained | 0.0798 | 0.4038      | 0.1686      | 0.4038   |
| TF_MBv2        | pretrained | 0.0869 | 0.3847      | 0.1783      | 0.3847   |

*** 

## References
Thanks for the authors' hard working of these repos
* [MalongTech/research-ms-loss](https://github.com/MalongTech/research-ms-loss) 
* [MalongTech/research-xbm](https://github.com/MalongTech/research-xbm)
* [pytorch-metric-learning](https://github.com/KevinMusgrave/pytorch-metric-learning)

## Licenses

Just copy licenses from official repos
*   MS-Loss is CC-BY-NC 4.0 licensed, as found in the [LICENSE](LICENSE) file. It is released for academic research / non-commercial use only. If you wish to use for commercial purposes, please contact sales@malongtech.com.
